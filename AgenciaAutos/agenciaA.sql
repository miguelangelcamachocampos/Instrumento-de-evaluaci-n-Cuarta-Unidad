-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-08-2017 a las 03:01:21
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agencia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autos`
--

CREATE TABLE `autos` (
  `placa` varchar(8) NOT NULL,
  `categoria` varchar(10) NOT NULL,
  `marca` varchar(10) NOT NULL,
  `modelo` varchar(10) NOT NULL,
  `year` int(11) NOT NULL,
  `color` varchar(8) NOT NULL,
  `renta` double NOT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `fotos` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `autos`
--

INSERT INTO `autos` (`placa`, `categoria`, `marca`, `modelo`, `year`, `color`, `renta`, `estado`, `tipo`, `fotos`) VALUES
('ABC-2345', 'Nuevo', 'BWM', 'M6', 2018, 'Blanco', 10000000, 'D', 'A', ''),
('B1405F', 'Clásico', 'BMW', 'I325', 2017, 'null', 20000, 'R', 'S', ''),
('B14556O', 'Seminuevo', 'CHevrolet', 'JETTA', 2003, 'Blanco', 6000, 'R', 'S', ''),
('GM1109F', 'Clásico', 'BMW', 'M6', 2017, 'null', 1000, 'R', 'S', ''),
('MIGUEL07', 'Nuevo', 'BMW', 'I3', 2008, 'Blanco', 1000, 'D', 'A', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(80) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `rfc` varchar(20) NOT NULL,
  `licencia` varchar(12) NOT NULL,
  `tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idCliente`, `nombre`, `direccion`, `telefono`, `email`, `rfc`, `licencia`, `tipo`) VALUES
(14, 'Miguel', 'Sn Miguel', '4181098987', 'miguel@gmail.com', 'CCMMAA9806', 'A1', 'FRECUENTE'),
(17, 'MIGUEL', 'GUERRERO', '23445', 'HBUJHYU', 'YUGBYU', 'BGYU', 'Frecuente'),
(18, 'MIGUEL ANGEL CAMACHO', 'LENADRO VALLE 108', '066', 'MIGUEL@GMAIL.COM', 'CCMA1998', 'AQDQDQD', 'VIP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `idmarca` int(11) NOT NULL,
  `marca` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`idmarca`, `marca`) VALUES
(5, 'FERRARI'),
(6, 'TOYOTA'),
(7, 'KIA'),
(14, 'BMW'),
(15, 'HONDA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos`
--

CREATE TABLE `modelos` (
  `Idmodelo` int(11) NOT NULL,
  `modelo` varchar(15) NOT NULL,
  `marca` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelos`
--

INSERT INTO `modelos` (`Idmodelo`, `modelo`, `marca`) VALUES
(1, 'TESTAROSA', 'FERRARI'),
(2, 'LA FERARRI', 'FERRARI'),
(3, 'CALIFORNIA', 'FERRARI'),
(4, 'ENZO', 'FERRARI'),
(5, 'CENTENARIO', 'FERRARI'),
(6, 'M6', 'BWM'),
(7, 'I8', 'BWM'),
(8, 'ISEETTA', 'BWM'),
(9, 'TURBO', 'BWM'),
(10, 'RIO', 'KIA'),
(12, 'PRONITRO', 'TOYOTA'),
(13, 'RIO2', 'KIA'),
(14, 'I3', 'BMW'),
(15, 'ASAS', 'TOYOTA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rentados`
--

CREATE TABLE `rentados` (
  `idrentado` int(11) NOT NULL,
  `placa` varchar(8) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `fecharenta` varchar(10) NOT NULL,
  `fechadevolucion` varchar(10) NOT NULL,
  `fechaentrega` varchar(10) NOT NULL,
  `monto` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autos`
--
ALTER TABLE `autos`
  ADD PRIMARY KEY (`placa`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`idmarca`);

--
-- Indices de la tabla `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`Idmodelo`);

--
-- Indices de la tabla `rentados`
--
ALTER TABLE `rentados`
  ADD PRIMARY KEY (`idrentado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `modelos`
--
ALTER TABLE `modelos`
  MODIFY `Idmodelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `rentados`
--
ALTER TABLE `rentados`
  MODIFY `idrentado` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
